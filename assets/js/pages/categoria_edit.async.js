$(document).ready(function(){
   makeRequestList();
});

function makeRequestList(){
    var jqxhr = $.ajax({
        type: 'GET',
        crossDomain: true,
        url: 'http://api-demo.localhost/categoria'
    })
    .done(function(json) { 
        makeRequest(json);
    });
}

function makeRequest(listadoCategoria){
    const params = new Proxy(new URLSearchParams(window.location.search), {
        get: (searchParams, prop) => searchParams.get(prop)
      });
      // Get the value of "some_key" in eg "https://example.com/?some_key=some_value"
      let id_categoria = params.id_categoria; // "some_value"

    var jqxhr = $.get( "http://api-demo.localhost/categoria/" + id_categoria, {}, function(data) {        
        
    })    
    .done(function(json) { 
        //debugger;
        //json = jQuery.parseJSON(data);
        console.log(json);
        $("#titulo").text("Editar categoría: " + json.nombre_categoria);
        
        var id_categoria = document.getElementById('id_categoria');
        id_categoria.setAttribute('value', json.id_categoria);
        
        var inputNombre = document.getElementById('nombre');
        inputNombre.setAttribute('value', json.nombre_categoria);
        var id_padre = (json.padre !== null) ? json.padre.id_categoria : undefined;
        var select = document.getElementById('id_padre');
     
        listadoCategoria.forEach(categoria => {            
            var option = document.createElement('option');
            option.setAttribute('value', categoria.id_categoria);
            if(categoria.id_categoria === id_padre){        
                option.setAttribute('selected', 'selected');
            }
            option.appendChild(document.createTextNode(getFullPath(categoria)));
            select.appendChild(option);
        });        
    })
    .fail(function(response) {           
        alert( "Error al realizar la petición: " + status + ": " + response.statusText );
    })
    .always(function() {
        
    }); 
}

function getFullPath(categoria){
    var fullPath = categoria.nombre_categoria;
    var padre = categoria.padre;
    while(padre !== null && padre !== undefined){
        fullPath = padre.nombre_categoria + " > " + fullPath;
        padre = padre.padre;
    }
    return fullPath;
}