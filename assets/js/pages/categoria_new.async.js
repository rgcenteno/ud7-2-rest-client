$(document).ready(function(){
    document.getElementById("guardar").addEventListener('click', saveForm, false);
});

function saveForm(click){
    var nombre = document.getElementById('nombre').value;
    var id_padre = document.getElementById('id_padre').value;
    if(nombre == ""){
        alert('Inserte un nombre para la categoría');
    }
    else{
        $.ajax({
            method: "POST",
            url: "http://localhost/20_21-DWES/ud7-1-rest/public/?controller=categoriaAsync",
            data: { 'id_padre' : id_padre, 'nombre' : nombre }
       })
      .done(function( msg ) {
          if(msg.guardado){
              location.href="http://localhost/20_21-DWES/ud7-1-rest/public/?controller=categoriaAsync";
          }
          else{
              alert('No se ha podido guardar el registro');
          }
      })
      .fail(function(msg){
          alert( "Error al realizar el guardado: " + msg.status + ": " + msg.statusText );
      });
  }
}