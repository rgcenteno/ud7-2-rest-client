//Para cambiar el API, reemplaza todas las cadenas http://localhost/20_21-DWES/ud7-1-rest/public/ por la URL de donde albergas el API

$(document).ready(function(){
   makeRequest();
});

function makeRequest(){
    var jqxhr = $.ajax({
        type: 'GET',
        crossDomain: true,
        url: 'http://api-demo.localhost/categoria'
    })
    .done(function(json) {                        
        json.forEach(rowCategoria);
        var deleteButtons = document.getElementsByClassName('btn-delete');
        for (var i = 0; i < deleteButtons.length; i++) {
            deleteButtons[i].addEventListener('click', deleteRow, false);
        }
    })
    .fail(function(response) {   
        alert( "Error al realizar la petición: " + status + ": " + response.statusText );
    })
    .always(function() {
        
    }); 
}


function deleteRow(click){
    var id_categoria = this.getAttribute('data-id_categoria');
    $.ajax({
        method: "DELETE",
        url: "http://api-demo.localhost/categoria/" + id_categoria
   })
  .done(function( msg ) {
        guiDeleteRow(id_categoria);      
      
  })
  .fail(function(msg){
      alert( "Error al realizar el borrado: " + msg.status + ": " + msg.statusText );
  });
}

async function guiDeleteRow(id_categoria){
    var trId = 'tr-'+id_categoria;
    console.log(trId);
    var tr = document.getElementById(trId);
    tr.setAttribute('class', 'table-danger');
    await sleep(1000);
    tr.remove();
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function showBreadcumb(key, element){
    var breadcumb = document.getElementById('breadcumb');
    breadcumb.appendChild(document.createTextNode(key));
}

function rowCategoria(categoria){
    var tbodyRef = document.getElementById('categoriaTable').getElementsByTagName('tbody')[0];
    var newRow = tbodyRef.insertRow();
    newRow.setAttribute('id', 'tr-' + categoria.id_categoria);

    // Insert a cell at the end of the row
    var newCell = newRow.insertCell();
    // Append a text node to the cell
    var nombre = document.createTextNode(categoria.nombre_categoria);
    newCell.appendChild(nombre);
    
    var cellPadre = newRow.insertCell();
    var nombrePadre;
    if(categoria.padre !== undefined && categoria.padre !== null){
        nombrePadre = document.createTextNode(categoria.padre.nombre_categoria);
    }
    else{
        nombrePadre = document.createTextNode('-');
    }
    cellPadre.appendChild(nombrePadre);
    
    var fullPath = categoria.nombre_categoria;
    var padre = categoria.padre;
    while(padre !== undefined && padre !== null){
        fullPath = padre.nombre_categoria + " > " + fullPath;
        padre = padre.padre;
    }
    
    var cellRuta = newRow.insertCell();
    var nombrePadre = document.createTextNode(fullPath);
    cellRuta.appendChild(nombrePadre);
    
    var cellOptions = newRow.insertCell();
    cellOptions.setAttribute('align', 'center');
    var aEdit = document.createElement('a');
    aEdit.setAttribute('class', 'btn btn-clock btn-outline-primary');
    aEdit.setAttribute('href', './categoria_edit.html?id_categoria='+categoria.id_categoria);
    var iEdit = document.createElement('i');
    iEdit.setAttribute('class', 'fas fa-edit');
    aEdit.appendChild(iEdit);
    
    var aDelete = document.createElement('a');
    aDelete.setAttribute('class', 'btn btn-clock btn-outline-danger btn-delete');
    aDelete.setAttribute('data-id_categoria', categoria.id_categoria);
    aDelete.setAttribute('href', '#'); //'./?controller=categoriaAsync&action=delete&id_categoria='+categoria.id_categoria
    var iDelete = document.createElement('i');
    iDelete.setAttribute('class', 'fas fa-trash');
    aDelete.appendChild(iDelete);
    
    //var nombrePadre = document.('<a class="btn btn-clock btn-outline-primary" href="./?controller=categoria&action=edit&id_categoria='+categoria.id_categoria+'"><i class="fas fa-edit"></i></a> <a class="btn btn-clock btn-outline-danger" href="./?controller=categoria&action=delete&id_categoria='+categoria.id_categoria+'"><i class="fas fa-trash"></i></a>');
    cellOptions.appendChild(aEdit);
    cellOptions.appendChild(document.createTextNode(' '));
    cellOptions.appendChild(aDelete);
}